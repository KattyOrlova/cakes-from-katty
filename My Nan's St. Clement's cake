Ingredients
125 g unsalted butter , softened, plus extra for greasing
225 g golden caster sugar
4 large free-range eggs
1 large orange
200 g ground almonds
100 g self-raising flour
LEMON ICING
225 g icing sugar
1 lemon

Method
Preheat your oven to 180°C/350°F/gas 4. Grease a 20cm loose-bottomed springform cake tin with a knob of butter, then line the base with greaseproof paper.
Beat the softened butter with 125g of the caster sugar until it’s light and creamy, then crack in the eggs, one at a time, beating each one in well before adding the next.
Finely grate in most of the orange zest, keeping back a few scrapings of the zest in a clingfilm-covered bowl. Fold in the ground almonds and sift in the flour. Mix and gently combine everything.
Spoon the cake batter into your prepared tin and bake in the oven for about 30 minutes, or until risen and lightly golden. To check that the cake is cooked through, poke a skewer or cocktail stick into the centre of the sponge. If it comes out clean, it’s done; if not, cook it for a few more minutes.
Leave the cake to cool for a few minutes in the tin while you make the orange syrup.
Put the remaining 100g of caster sugar into a pan and add the juice of the zested orange. Place the pan on a medium heat for a few minutes, or until the sugar has dissolved.
While the cake is still hot, poke lots of little holes in the top with a cocktail stick and pour the syrup all over it. Once all the syrup has been absorbed, move the cake to a wire rack to cool completely.
To make the icing, sift the icing sugar into a bowl and grate in most of the lemon zest. Keep back a few gratings, add them to the bowl of reserved orange zest, and cover again with clingfilm. Squeeze the lemon juice over the icing sugar and mix, adding more juice if needed until you get a good drizzling consistency.
When the cake has completely cooled, transfer it to a serving plate and pour that lemony icing all over the top, letting it drizzle down the sides. Sprinkle over the reserved orange and lemon zest, and serve.