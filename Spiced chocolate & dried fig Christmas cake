5 oranges
50 ml vegetable oil
50 ml whisky or brandy
150 g clear runny honey
2 large free-range eggs
260 g self-raising flour
200 g dark brown sugar
50 g ground hazelnuts or almonds
2 teaspoons ground cinnamon
1 teaspoon ground star anise
1 teaspoon ground ginger
2 tablespoons quality cocoa powder
100 g quality dark chocolate (70%)
300 g dried figs
120 g hazelnuts
50 g crystalised ginger
CHOCOLATE GLAZE
25 ml clear runny honey
50 g caster sugar
25 ml whisky or brandy
1 orange
1 tablespoon orange blossom water
125 g quality dark chocolate (70%)
75 g unsalted butter (at room temperature) , plus extra for greasing

Method
Preheat the oven to 180ºC/350ºF/gas 4.
Finely grate the zest of 2 oranges into a large bowl, then squeeze in the juice of all 4 - you need about 150ml.
Measure the oil, brandy and honey into the bowl. Crack in the eggs into a mug, lightly beat with a fork, then tip into the bowl.
Sift the flour into a separate large bowl, then add the sugar, nuts, spices, cocoa powder and big pinch of sea salt.
Chop the chocolate, halve the dried figs, and then roughly chop the hazelnuts. Finely dice the crystalised ginger, then tip into the bowl of dry ingredients, along wth the figs, nuts and chocolate.
Pour the wet mixture over the dry ingredients and fold until well combined (be sure not to overwork it as the cake may become a bit too dense).
Transfer to a buttered tin and place in the centre of the oven for 25 minutes. Rotate the tin in the oven, then continue to bake for another 10 to 15 minutes.
Test the cake by briefly removing it from the oven and pressing gently in the centre – it should feel rather firm without any sinking. (The skewer test won’t work here as there is so much fruit and chocolate.) Then listen to the cake: it should be silent; if you hear a crackling or hissing, bake for a little longer.
Remove from the oven and allow to cool in the tin for 15 to 30 minutes. Flip to release from the tin. (If using a springform tin, you can leave it to cool completely in the tin before releasing, but bundts are best released while the cake is still warm.)
To prepare the chocolate glaze, place the honey, sugar and whisky or brandy in a pan. Squeeze in half the orange juice, then measure in the orange blossom water, and 40ml of cold water. Bring to a rapid boil for 1 minute, then remove from the heat. Rest for 3 minutes.
Finely chop the chocolate and dice the butter. Add the chocolate to the pan and whisk to combine, until melted.
Next, add the butter a little at a time, mixing continuously until everything is melted and looking lovely and shiny.
Allow the glaze to cool slightly before drizzling all over your cake to serve (you can make the glaze a day ahead if you like – gently warm it up just before drizzling).

Tips
Bake in a large bundt cake tin for the best ratio of glaze to cake. If you don’t have one, a 23cm springform tin will do.